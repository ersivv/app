#include <SPI.h>

#include "App.h"

#ifdef ESP8266
extern "C" {
#include "user_interface.h"
}
#endif

//#include <Adafruit_ILI9341esp\Adafruit_ILI9341esp.h>
//#include <Adafruit_GFX_Library\Adafruit_GFX.h>
//#include <XPT2046\XPT2046.h>
//
//#define TFT_DC 2
//#define TFT_CS 15
//
//Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
//Adafruit_GFX_Button button;
//
//XPT2046 touch(/*cs=*/ 16, /*irq=*/ 3);

void setup() {



	if (UApp._debug) {
		UApp.InitHardwareSerial();
	}

	UApp.Debugln("Make HBPro.ru v3.0.0");
	UApp.Debugln("----------------------");
	UApp.Debugln("Loading Uniholic");
	UApp.Debugln("----------------------");

	UApp.Debug_tm(); UApp.Debugln(" Init Gfx");
	UApp.InitGfx();

	uint64_t t = Time::getMillis();
	UApp._gfx->fillScreen(RGB565_BLACK);
	uint32_t d = Time::getMillis() - t;

	UApp.Debugln(" Fill screen: " + (String)d + " ms");

	//UApp._gfx->drawCircleHelper(100, 100, 70, 3, RGB565_GREEN);
	//UApp._gfx->fillRoundRect(10, 10, 150, 100, 10, RGB565_MAGENTA);
	//UApp._gfx->fillTriangle(10, 315, 200, 318, 35, 200, RGB565_BLUE);
	UApp.Debugln("Touch_cal_x_0: " + (String)UApp._gfx->_touch->_touch_cal_x_0);
	UApp.Debugln("Touch_cal_y_0: " + (String)UApp._gfx->_touch->_touch_cal_y_0);
	UApp.Debugln("Touch_cal_x_max: " + (String)UApp._gfx->_touch->_touch_cal_x_max);
	UApp.Debugln("Touch_cal_y_max: " + (String)UApp._gfx->_touch->_touch_cal_y_max);

	UApp._gfx->drawPixel(0, 0, RGB565_GREEN);
	//UApp._gfx->drawPixel(1, 1, RGB565_ORANGE);

	UApp._gfx->drawPixel(239, 0, RGB565_GREEN);
	//UApp._gfx->drawPixel(239, 1, RGB565_ORANGE);

	UApp._gfx->drawPixel(0, 319, RGB565_BLUE);
	UApp._gfx->drawPixel(239, 319, RGB565_BLUE);


}


void loop() {


	//UApp.Debugln(" " + (String)ESP.getFreeHeap());
	if (UApp._gfx->_touch->isTouching())
	{
		lcd_point_t xy = UApp._gfx->_touch->getRawXY();
		if (xy != lcd_point_t{ 0xFFFF,0xFFFF })
		{
			UApp.Debug_tm();
			UApp.Debugln(" X: " + (String)xy.x + " Y: " + (String)xy.y);
			//delete &xy;
		}
		else
		{
			UApp.Debug_tm();
			UApp.Debugln(" XY out of range");
		}
	}
	//delay(20);
	//UApp.Debugln(system_get_time());
	//UApp.Debugln(system_get_free_heap_size());


}



