#include "XPT2046.h"

XPT2046::XPT2046()
{
}


XPT2046::~XPT2046()
{
}

XPT2046::XPT2046(uint8_t cs, uint8_t irq)
	:_cs(cs),
	_irq(irq),
	_precision(TOUCH_PREC_MEDIUM)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// ”становить точность измерени¤.
///
/// @author	Ers
/// @date	13.02.2016
///
/// @param	p	¬ариант кол-ва измерений.
////////////////////////////////////////////////////////////////////////////////////////////////////
void XPT2046::setPrecision(precision_t p)
{
	_precision = p;
}



void XPT2046::begin() {
	pinMode(_cs, OUTPUT);
	pinMode(_irq, INPUT_PULLUP);

	// Default calibration (map 0..TOUCH_ADC_MAX -> 0..width|height)
	//setCalibration(
	//	/*vi1=*/((int32_t)TOUCH_CAL_MARGIN) * TOUCH_ADC_MAX / width,
	//	/*vj1=*/((int32_t)TOUCH_CAL_MARGIN) * TOUCH_ADC_MAX / height,
	//	/*vi2=*/((int32_t)width - TOUCH_CAL_MARGIN) * TOUCH_ADC_MAX / width,
	//	/*vj2=*/((int32_t)height - TOUCH_CAL_MARGIN) * TOUCH_ADC_MAX / height
	//	);
	// TODO(?) Use the following empirical calibration instead? -- Does it depend on VCC??
	// touch.setCalibration(209, 1759, 1775, 273);


	powerDown();  // Make sure PENIRQ is enabled
}


void XPT2046::powerDown() const {
	digitalWrite(_cs, LOW);
	HSPI.transfer(TOUCH_CTRL_HI_Y | TOUCH_CTRL_LO_SER);
	HSPI.transfer16(0);  // Flush, just to be sure
	digitalWrite(_cs, HIGH);
}


// TODO: Caveat - MODE_SER is completely untested!!
//   Need to measure current draw and see if it even makes sense to keep it as an option
//void XPT2046::getRaw(uint16_t &vi, uint16_t &vj, adc_ref_t mode, uint8_t max_samples) const {
uint16_t XPT2046::getRaw(precision_t precision, adc_ref_t mode)
{
	// Implementation based on TI Technical Note http://www.ti.com/lit/an/sbaa036/sbaa036.pdf
	uint16_t tx = 0;
	uint16_t ty = 0;

	uint8_t ctrl_lo = ((mode == MODE_DFR) ? TOUCH_CTRL_LO_DFR : TOUCH_CTRL_LO_SER);

	digitalWrite(_cs, LOW);
	

	for (uint8_t i = 0; i<(uint8_t)precision; i++)
	{
		uint16_t temp_x = 0;
		uint16_t temp_y = 0;

		HSPI.transfer(TOUCH_CTRL_HI_X | ctrl_lo);
		temp_x = (HSPI.transfer(0) << 4) | (HSPI.transfer(TOUCH_CTRL_HI_X | ctrl_lo) >> 4);

		//os_printf("%c", x_temp);

		HSPI.transfer(TOUCH_CTRL_HI_Y | ctrl_lo);
		temp_y = (HSPI.transfer(0) << 4) | (HSPI.transfer(TOUCH_CTRL_HI_Y | ctrl_lo) >> 4);

		//if (!((x_temp>max(touch_x_left, touch_x_right)) or (x_temp == 0) or (y_temp>max(touch_y_top, touch_y_bottom)) or (y_temp == 0)))
		//{
		//	ty += x_temp;
		//	tx += y_temp;
		//	datacount++;
		//}
		tx = temp_x;
	}

	if (mode == MODE_DFR) {
		// Turn off ADC by issuing one more read (throwaway)
		// This needs to be done, because PD=0b11 (needed for MODE_DFR) will disable PENIRQ
		HSPI.transfer(0);  // Maintain 16-clocks/conversion; _readLoop always ends after issuing a control byte
		HSPI.transfer(TOUCH_CTRL_HI_Y | TOUCH_CTRL_LO_SER);
	}
	HSPI.transfer16(0);  // Flush last read, just to be sure

	digitalWrite(_cs, HIGH);
	return tx;
}


bool XPT2046::isTouching() const
{
	return (digitalRead(_irq) == LOW);
}

lcd_point_t XPT2046::getRawXY(precision_t precision, adc_ref_t mode)
{
	if (!isTouching()) {
		return lcd_point_t {0xFFFF, 0xFFFF};
	}

	// Implementation based on TI Technical Note http://www.ti.com/lit/an/sbaa036/sbaa036.pdf
	uint16_t x_raw = 0xFFFF;
	uint16_t y_raw = 0xFFFF;

	uint8_t ctrl_lo = ((mode == MODE_DFR) ? TOUCH_CTRL_LO_DFR : TOUCH_CTRL_LO_SER);

	digitalWrite(_cs, LOW);

	uint8_t i = 0;
	do {
		uint16_t x_temp = 0;
		uint16_t y_temp = 0;

		HSPI.transfer(TOUCH_CTRL_HI_X | ctrl_lo);
		x_temp = (HSPI.transfer(0) << 4) | (HSPI.transfer(TOUCH_CTRL_HI_X | ctrl_lo) >> 4);

		HSPI.transfer(TOUCH_CTRL_HI_Y | ctrl_lo);
		y_temp = (HSPI.transfer(0) << 4) | (HSPI.transfer(TOUCH_CTRL_HI_Y | ctrl_lo) >> 4);

		if ((x_temp > _touch_cal_x_0) && (x_temp < _touch_cal_x_max))
		{
			if (x_raw == 0xFFFF)
			{
				x_raw = x_temp;
			}
			else
			{
				x_raw = (x_raw + x_temp) / 2;
			};
		};

		if ((y_temp > _touch_cal_y_0) && (y_temp < _touch_cal_y_max))
		{
			if (y_raw == 0xFFFF)
			{
				y_raw = y_temp;
			}
			else
			{
				x_raw = (y_raw + y_temp) / 2;
			};
		};


	} while (isTouching() && (++i < precision));


	if (mode == MODE_DFR) {
		// Turn off ADC by issuing one more read (throwaway)
		// This needs to be done, because PD=0b11 (needed for MODE_DFR) will disable PENIRQ
		HSPI.transfer(0);  // Maintain 16-clocks/conversion; _readLoop always ends after issuing a control byte
		HSPI.transfer(TOUCH_CTRL_HI_Y | TOUCH_CTRL_LO_SER);
	}
	HSPI.transfer16(0);  // Flush last read, just to be sure

	digitalWrite(_cs, HIGH);

	if (x_raw == 0xFFFF || y_raw == 0xFFFF)
	{
		return lcd_point_t{ 0xFFFF, 0xFFFF };
	}
	else
	{
		return lcd_point_t{ x_raw,y_raw };
	}

	
}
