#include "Lcd.h"


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Массив возможных используемых LCD
////////////////////////////////////////////////////////////////////////////////////////////////////
const lcd_model_t Lcd::LCD_MODEL_ARR[] =
{
	{ LCD_MODEL::UNKNOWN ,"UNKNOWN",0,0 },
	{ LCD_MODEL::ILI9431 ,"ILI9341",320,240 }
};


Lcd::Lcd()
{
}

Lcd::~Lcd()
{
}

uint8_t Lcd::getPinCs() {
	return _cs;
}

uint8_t Lcd::getPinDc() {
	return _dc;
}


LCD_MODEL Lcd::getID() {
	LCD_MODEL model;
	model = ILI9431;
	return model;
};


LCD_MODEL Lcd::getModel() {
	return _model;
};

void Lcd::setModel(LCD_MODEL model) {
	_model = model;
};


uint16_t Lcd::getWidth() {
	return _width;
};


void Lcd::setWidth(uint16_t width) {
	_width = width;
};

uint16_t Lcd::getHeight() {
	return _height;
};

void Lcd::setHeight(uint16_t height) {
	_height = height;
};

bool Lcd::setDefineLcd() {
	LCD_MODEL model = getID();
	if (model) {
		setModel(model);
		switch (model)
		{
		case LCD_MODEL::ILI9431:
		{
			_width = 240;
			_height = 320;
		}
		break;
		case LCD_MODEL::UNKNOWN:
			break;
		}
		return true;
	}
	else
	{
		return false;
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Constructor.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	cs 	The create struct.
/// @param	dc 	The device-context.
/// @param	rst	The reset.
////////////////////////////////////////////////////////////////////////////////////////////////////
Lcd::Lcd(int8_t cs, int8_t dc, int8_t rst) {
	_cs = cs;
	_dc = dc;
	//_rst = rst;
	_csMask = digitalPinToBitMask(_cs);
	_dcMask = digitalPinToBitMask(_dc);
	//_rstMask = digitalPinToBitMask(_rst);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Writecommands the given command.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	command	The command.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Lcd::writecommand(uint8_t command) {

	HSPI.spiLow(_dc);
	HSPI.spiLow(_cs);

	HSPI.write(command);

	HSPI.spiHigh(_cs);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Writedata the given data.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	data	The data.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Lcd::writedata(uint8_t data) {
	
	HSPI.spiHigh(_dc);
	HSPI.spiLow(_cs);

	HSPI.write(data);

	HSPI.spiHigh(_cs);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Writedata.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param [in,out]	data	If non-null, the data.
/// @param	size			The size.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Lcd::writedata(uint8_t * data, uint8_t size) {
	
	HSPI.spiHigh(_dc);
	HSPI.spiLow(_cs);

	HSPI.writeBytes(data, size);

	HSPI.spiHigh(_cs);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Writes a command data.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	command			The command.
/// @param [in,out]	data	If non-null, the data.
/// @param	size			The size.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Lcd::writeCmdData(uint8_t command, uint8_t * data, uint8_t size) {
	HSPI.spiLow(_dc);
	HSPI.spiLow(_cs);

	HSPI.write(command);

	HSPI.spiHigh(_dc);

	HSPI.writeBytes(data, size);

	HSPI.spiHigh(_cs);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Gets the readdata.
///
/// @author	Ers
/// @date	13.02.2016
///
/// @return	An uint8_t.
////////////////////////////////////////////////////////////////////////////////////////////////////
uint8_t Lcd::readdata(void) {
	HSPI.spiBegin();
	HSPI.spiLow(_cs);
	HSPI.spiLow(_dc);
	uint8_t r = HSPI.transfer(0x00);
	HSPI.spiHigh(_cs);
	HSPI.spiEnd();
	return r;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Readcommand 8.
///
/// @author	Ers
/// @date	13.02.2016
///
/// @param	command	The command.
/// @param	index  	Zero-based index of the.
///
/// @return	An uint8_t.
////////////////////////////////////////////////////////////////////////////////////////////////////
uint8_t Lcd::readcommand8(uint8_t command, uint8_t index) {
	HSPI.spiBegin();

	HSPI.spiLow(_cs);
	HSPI.spiLow(_dc);

	HSPI.write(0xD9);  // woo sekret command?
	HSPI.spiHigh(_dc);
	HSPI.write(0x10 + index);

	HSPI.spiLow(_dc);
	HSPI.write(command);

	HSPI.spiHigh(_dc);
	uint8_t r = HSPI.transfer(0x00);
	HSPI.spiHigh(_cs);

	HSPI.spiEnd();
	return r;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Инициализация LCD.
///
/// @author	Ers
/// @date	11.02.2016
////////////////////////////////////////////////////////////////////////////////////////////////////
void Lcd::begin(void) {

	pinMode(_dc, OUTPUT);
	pinMode(_cs, OUTPUT);

	HSPI.begin();

	HSPI.spiBegin();

	writeCmdDataTmp(0xEF, 0x03, 0x80, 0x02);
	writeCmdDataTmp(0xCF, 0x00, 0XC1, 0X30);
	writeCmdDataTmp(0xED, 0x64, 0x03, 0X12, 0X81);
	writeCmdDataTmp(0xE8, 0x85, 0x00, 0x78);
	writeCmdDataTmp(0xCB, 0x39, 0x2C, 0x00, 0x34, 0x02);
	writeCmdDataTmp(0xF7, 0x20);
	writeCmdDataTmp(0xEA, 0x00, 0x00);

	//Powercontrol
	//VRH[5:0]
	writeCmdDataTmp(ILI9341_PWCTR1, 0x23);

	//Powercontrol
	//SAP[2:0];BT[3:0]
	writeCmdDataTmp(ILI9341_PWCTR2, 0x10);

	//VCMcontrol
	writeCmdDataTmp(ILI9341_VMCTR1, 0x3e, 0x28);

	//VCMcontrol2
	writeCmdDataTmp(ILI9341_VMCTR2, 0x86);

	//MemoryAccessControl
	writeCmdDataTmp(ILI9341_MADCTL, 0x48);

	writeCmdDataTmp(ILI9341_PIXFMT, 0x55);
	writeCmdDataTmp(ILI9341_FRMCTR1, 0x00, 0x18);

	//DisplayFunctionControl
	writeCmdDataTmp(ILI9341_DFUNCTR, 0x08, 0x82, 0x27);

	//3GammaFunctionDisable
	writeCmdDataTmp(0xF2, 0x00);

	//Gammacurveselected
	writeCmdDataTmp(ILI9341_GAMMASET, 0x01);

	//SetGamma
	writeCmdDataTmp(ILI9341_GMCTRP1, 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08, 0x4E, 0xF1, 0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00);
	writeCmdDataTmp(ILI9341_GMCTRN1, 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07, 0x31, 0xC1, 0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F);

	writecommand(ILI9341_SLPOUT);    //Exit Sleep 

	HSPI.spiEnd();
	//delay(120);

	HSPI.spiBegin();
	writecommand(ILI9341_DISPON);    //Display on 

	HSPI.spiEnd();

}