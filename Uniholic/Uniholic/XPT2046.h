#pragma once

#include "Uhspi.h"

#define TOUCH_CAL_MARGIN	(uint16_t)20
#define TOUCH_ADC_MAX		(uint16_t)0x0fff

#define TOUCH_CTRL_LO_DFR	(uint8_t)0b0011
#define TOUCH_CTRL_LO_SER	(uint8_t)0b0100
#define TOUCH_CTRL_HI_X		(uint8_t)(0b1001 << 4)
#define TOUCH_CTRL_HI_Y		(uint8_t)(0b1101 << 4)

struct lcd_point_t
{
	uint16_t x;
	uint16_t y;

	bool operator == (const lcd_point_t &point) const {
		return this->x == point.x && this->y == point.y;
	}

	bool operator != (const lcd_point_t &point) const {
		return this->x != point.x || this->y != point.y;
	}
};



class XPT2046
{
public:

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Определяет тип входа АЦП.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	enum adc_ref_t : uint8_t
	{
		MODE_SER,	//несимметричный
		MODE_DFR	//дифференциальный 
	};

	enum precision_t : uint8_t
	{
		TOUCH_PREC_LOW		= 1,
		TOUCH_PREC_MEDIUM	= 10,
		TOUCH_PREC_HARD		= 20,
		TOUCH_PREC_EXTREME	= 100
	};

	XPT2046();
	~XPT2046();

	XPT2046(uint8_t cs, uint8_t irq);

	void begin();

	void powerDown() const;

	bool isTouching() const;

	lcd_point_t getRawXY(precision_t p = TOUCH_PREC_EXTREME, adc_ref_t mode = MODE_DFR);


	int16_t _touch_cal_x_0;
	int16_t _touch_cal_y_0;
	int16_t _touch_cal_x_max;
	int16_t _touch_cal_y_max;

private:

	//static const uint16_t TOUCH_CAL_MARGIN = 20;
	//static const uint16_t TOUCH_ADC_MAX = 0x0fff;  // 12 bits

	//static const uint8_t TOUCH_CTRL_LO_DFR = 0b0011;
	//static const uint8_t TOUCH_CTRL_LO_SER = 0b0100;
	//static const uint8_t TOUCH_CTRL_HI_X = 0b1001 << 4;
	//static const uint8_t TOUCH_CTRL_HI_Y = 0b1101 << 4;



	void setPrecision(precision_t p);

	//void setCalibration(uint16_t vi1, uint16_t vj1, uint16_t vi2, uint16_t vj2, uint16_t width, uint16_t height);
	uint16_t getRaw(precision_t p = TOUCH_PREC_MEDIUM, adc_ref_t mode = MODE_DFR);

	

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Пин CS TOUCH LCD.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	int8_t _cs;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Пин IRQ TOUCH LCD.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	int8_t _irq;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Точность определения (кол-во измерений для усреднения).
	////////////////////////////////////////////////////////////////////////////////////////////////////
	precision_t _precision;




};

