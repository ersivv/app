#pragma once

#include "App.h"

class Time
{
public:
	Time();
	~Time();

	static uint64_t getMillis();
	static String toStringDBG(uint64_t t);
	
};

