#include "Uhspi.h"

Uhspi HSPI;

Uhspi::Uhspi()
{
}


Uhspi::~Uhspi()
{
}

void Uhspi::begin()
{
	_spi->begin();
}

void Uhspi::spiBegin()// __attribute__((always_inline))
{
	beginTransaction(_spiSetting);
}

void Uhspi::spiEnd()// __attribute__((always_inline))
{
	endTransaction();
}

//void Uhspi::spiMask(uint8_t pin)
//{
//	GPOS = digitalPinToBitMask(pin);
//}

void Uhspi::spiLow(uint8_t pin)
{
	GPOC = digitalPinToBitMask(pin);
}

void Uhspi::spiHigh(uint8_t pin)
{
	GPOS = digitalPinToBitMask(pin);
}
