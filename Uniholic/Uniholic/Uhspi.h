#pragma once

#include <SPI.h>

class Uhspi : public SPIClass
{
public:

	SPIClass* _spi = &SPI;
	SPISettings _spiSetting = SPISettings(8000000, MSBFIRST, SPI_MODE0);

	void begin();
	void spiBegin();
	void spiEnd();
	void spiLow(uint8_t pin);
	void spiHigh(uint8_t pin);

	Uhspi();
	~Uhspi();
};

extern Uhspi HSPI;