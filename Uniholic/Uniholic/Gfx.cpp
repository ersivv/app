#include "Gfx.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default constructor.
///
/// @author	Ers
/// @date	11.02.2016
////////////////////////////////////////////////////////////////////////////////////////////////////
Gfx::Gfx()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Destructor.
///
/// @author	Ers
/// @date	11.02.2016
////////////////////////////////////////////////////////////////////////////////////////////////////
Gfx::~Gfx()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Установить поворот LCD.
/// 
/// @author	Ers
/// @date	13.02.2016
///
/// @param	r	Тип поворота LCD_ROTATION_DEGREE 0 90 180 270.
/// 			Для использования int i преобразование static_cast<rotation>(i)
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::setRotation(rotation_t r) {

	HSPI.spiBegin();
	_lcd->writecommand(ILI9341_MADCTL);
	uint8_t rot = (uint8_t)r % 4;

	switch (rot) {
	case 0:
		_lcd->writedata(ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR);
		_width = LCD_WIDTH;
		_height = LCD_HEIGHT;
		_rotation = LCD_ROTATION_DEGREE_0;
		break;
	case 1:
		_lcd->writedata(ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR);
		_width = LCD_HEIGHT;
		_height = LCD_WIDTH;
		_rotation = LCD_ROTATION_DEGREE_90;
		break;
	case 2:
		_lcd->writedata(ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR);
		_width = LCD_WIDTH;
		_height = LCD_HEIGHT;
		_rotation = LCD_ROTATION_DEGREE_180;
		break;
	case 3:
		_lcd->writedata(ILI9341_MADCTL_MX | ILI9341_MADCTL_MY | ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR);
		_width = LCD_HEIGHT;
		_height = LCD_WIDTH;
		_rotation = LCD_ROTATION_DEGREE_270;
		break;
	}
	HSPI.spiEnd();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Включить инверсию LCD.
///
/// @author	Ers
/// @date	13.02.2016
///
/// @param	i
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::invertDisplay(boolean i) {
	HSPI.spiBegin();
	_lcd->writecommand(i ? ILI9341_INVON : ILI9341_INVOFF);
	HSPI.spiEnd();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Преобразовать цвет RGB565.
///
/// @author	Ers
/// @date	13.02.2016
///
/// @param	r	Красная составляющая цвета.
/// @param	g	Зеленая составляющая цвета.
/// @param	b	Синяя составляющая цвета.
///
/// @return	Цвет 16-bit.
////////////////////////////////////////////////////////////////////////////////////////////////////
uint16_t Gfx::color565(uint8_t r, uint8_t g, uint8_t b) {
	return ((r & 0xF8) << 8) | ((g & 0xFC) << 3) | (b >> 3);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Sets address window.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	x0	The x coordinate 0.
/// @param	y0	The y coordinate 0.
/// @param	x1	The first x value.
/// @param	y1	The first y value.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::setAddressWindow_(
	uint16_t x0, uint16_t y0,
	uint16_t x1, uint16_t y1)
{

	uint8_t buffC[] = { (uint8_t)(x0 >> 8), (uint8_t)x0, (uint8_t)(x1 >> 8), (uint8_t)x1 };
	uint8_t buffP[] = { (uint8_t)(y0 >> 8), (uint8_t)y0, (uint8_t)(y1 >> 8), (uint8_t)y1 };

	HSPI.spiLow(LCD_DC);
	HSPI.write(ILI9341_CASET);
	HSPI.spiHigh(LCD_DC);
	HSPI.writeBytes(&buffC[0], sizeof(buffC));

	HSPI.spiLow(LCD_DC);
	HSPI.write(ILI9341_PASET);
	HSPI.spiHigh(LCD_DC);
	HSPI.writeBytes(&buffP[0], sizeof(buffP));

	HSPI.spiLow(LCD_DC);
	HSPI.write(ILI9341_RAMWR);
	HSPI.spiHigh(LCD_DC);

}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Sets address window.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	x0	The x coordinate 0.
/// @param	y0	The y coordinate 0.
/// @param	x1	The first x value.
/// @param	y1	The first y value.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::setAddressWindow(
	uint16_t x0, uint16_t y0,
	uint16_t x1, uint16_t y1)
{
	HSPI.spiLow(LCD_CS);
	setAddressWindow_(x0, y0, x1, y1);
	HSPI.spiHigh(LCD_CS);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать пиксель.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	x	 	Координата X.
/// @param	y	 	Координата Y.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawPixel(int16_t x, int16_t y, uint16_t color)
{

	if ((x < 0) || (x >= LCD_WIDTH) || (y < 0) || (y >= LCD_HEIGHT))
		return;

	HSPI.spiBegin();
	HSPI.spiLow(LCD_CS);

	setAddressWindow_(x, y, x + 1, y + 1);

	HSPI.write16(color);
	HSPI.spiHigh(LCD_CS);
	HSPI.spiEnd();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать отрезок.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	x0   	Координата начала X.
/// @param	y0   	Координата начала Y.
/// @param	x1   	Координата конца X.
/// @param	y1   	Координата конца Y.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawLine(
	int16_t x0, int16_t y0,
	int16_t x1, int16_t y1,
	uint16_t color)
{
	int16_t steep = abs(y1 - y0) > abs(x1 - x0);
	if (steep)
	{
		swap(x0, y0);
		swap(x1, y1);
	}

	if (x0 > x1)
	{
		swap(x0, x1);
		swap(y0, y1);
	}

	int16_t dx, dy;
	dx = x1 - x0;
	dy = abs(y1 - y0);

	int16_t err = dx / 2;
	int16_t ystep;

	if (y0 < y1)
	{
		ystep = 1;
	}
	else
	{
		ystep = -1;
	}

	for (; x0 <= x1; x0++)
	{
		if (steep) {
			drawPixel(y0, x0, color);
		}
		else
		{
			drawPixel(x0, y0, color);
		}
		err -= dy;
		if (err < 0)
		{
			y0 += ystep;
			err += dx;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать вертикальный отрезок.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x   	Координата начала X.
/// @param	y	  	Координата начала Y.
/// @param	h	 	Длина по вертикали.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawFastVLine(
	int16_t x, int16_t y,
	int16_t h,
	uint16_t color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT))
		return;
	if ((y + h - 1) >= LCD_HEIGHT)
		h = LCD_HEIGHT - y;

	HSPI.spiBegin();
	HSPI.spiLow(LCD_CS);

	setAddressWindow_(x, y, x, (y + h - 1));
	uint8_t colorBin[] = { (uint8_t)(color >> 8), (uint8_t)color };

	HSPI.writePattern(&colorBin[0], 2, h);

	HSPI.spiHigh(LCD_CS);
	HSPI.spiEnd();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать горизонтальный отрезок.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x   	Координата начала X.
/// @param	y   	Координата начала Y.
/// @param	w	 	Длина по горизонтали.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawFastHLine(
	int16_t x, int16_t y,
	int16_t w, uint16_t color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT))
		return;
	if ((x + w - 1) >= LCD_WIDTH)
		w = LCD_WIDTH - x;


	HSPI.spiBegin();
	HSPI.spiHigh(LCD_DC);
	HSPI.spiLow(LCD_CS);

	setAddressWindow_(x, y, (x + w - 1), y);

	uint8_t colorBin[] = { (uint8_t)(color >> 8), (uint8_t)color };
	HSPI.writePattern(&colorBin[0], 2, w);

	HSPI.spiHigh(LCD_CS);
	HSPI.spiEnd();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать окружность.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x0   	Координата центра X.
/// @param	y0	  	Координата центра Y.
/// @param	r	 	Радиус.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawCircle(
	int16_t x0, int16_t y0,
	int16_t r,
	uint16_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	drawPixel(x0, y0 + r, color);
	drawPixel(x0, y0 - r, color);
	drawPixel(x0 + r, y0, color);
	drawPixel(x0 - r, y0, color);

	while (x < y)
	{
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		drawPixel(x0 + x, y0 + y, color);
		drawPixel(x0 - x, y0 + y, color);
		drawPixel(x0 + x, y0 - y, color);
		drawPixel(x0 - x, y0 - y, color);
		drawPixel(x0 + y, y0 + x, color);
		drawPixel(x0 - y, y0 + x, color);
		drawPixel(x0 + y, y0 - x, color);
		drawPixel(x0 - y, y0 - x, color);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать дуги.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x0		  	Координата центра X.
/// @param	y0		  	Координата центра Y.
/// @param	r		  	Радиус.
/// @param	cornername	The cornername.
/// @param	color	  	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawCircleHelper(
	int16_t x0, int16_t y0,
	int16_t r,
	uint8_t cornername,
	uint16_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;
		if (cornername & 0x4)
		{
			drawPixel(x0 + x, y0 + y, color);
			drawPixel(x0 + y, y0 + x, color);
		}
		if (cornername & 0x2)
		{
			drawPixel(x0 + x, y0 - y, color);
			drawPixel(x0 + y, y0 - x, color);
		}
		if (cornername & 0x8)
		{
			drawPixel(x0 - y, y0 + x, color);
			drawPixel(x0 - x, y0 + y, color);
		}
		if (cornername & 0x1)
		{
			drawPixel(x0 - y, y0 - x, color);
			drawPixel(x0 - x, y0 - y, color);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать треугольник.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x0   	Кордината 1-й вершины X.
/// @param	y0   	Кордината 1-й вершины Y.
/// @param	x1   	Кордината 2-й вершины X.
/// @param	y1   	Кордината 2-й вершины Y.
/// @param	x2   	Кордината 3-й вершины X.
/// @param	y2   	Кордината 3-й вершины Y.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawTriangle(
	int16_t x0, int16_t y0,
	int16_t x1, int16_t y1,
	int16_t x2, int16_t y2,
	uint16_t color)
{
	drawLine(x0, y0, x1, y1, color);
	drawLine(x1, y1, x2, y2, color);
	drawLine(x2, y2, x0, y0, color);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать прямоугольник.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x   	Координата начала X.
/// @param	y	  	Координата начала Y.
/// @param	w	 	Длина по горизонтали.
/// @param	h	 	Длина по вертикали.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawRect(
	int16_t x, int16_t y,
	int16_t w, int16_t h,
	uint16_t color)
{
	drawFastHLine(x, y, w, color);
	drawFastHLine(x, y + h - 1, w, color);
	drawFastVLine(x, y, h, color);
	drawFastVLine(x + w - 1, y, h, color);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать прямоугольник с скругленными краями.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x   	Координата начала X.
/// @param	y	  	Координата начала Y.
/// @param	w	 	Длина по горизонтали.
/// @param	h	 	Длина по вертикали.
/// @param	r	 	Радиус скругления углов.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::drawRoundRect(
	int16_t x, int16_t y,
	int16_t w, int16_t h,
	int16_t r,
	uint16_t color)
{
	//прямоугольные части
	drawFastHLine(x + r, y, w - 2 * r, color);			// верхяя
	drawFastHLine(x + r, y + h - 1, w - 2 * r, color);	// нижняя
	drawFastVLine(x, y + r, h - 2 * r, color);			// левая
	drawFastVLine(x + w - 1, y + r, h - 2 * r, color);	// правая

	//скругленные углы
	drawCircleHelper(x + r, y + r, r, 1, color);
	drawCircleHelper(x + w - r - 1, y + r, r, 2, color);
	drawCircleHelper(x + w - r - 1, y + h - r - 1, r, 4, color);
	drawCircleHelper(x + r, y + h - r - 1, r, 8, color);
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать круг.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x0   	Координата центра X.
/// @param	y0	  	Координата центра Y.
/// @param	r	 	Радиус.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::fillCircle(
	int16_t x0, int16_t y0,
	int16_t r,
	uint16_t color)
{
	drawFastVLine(x0, y0 - r, 2 * r + 1, color);
	fillCircleHelper(x0, y0, r, 3, 0, color);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать сектора круга.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x0		  	Координата центра X.
/// @param	y0		  	Координата центра Y.
/// @param	r		  	Радиус.
/// @param	cornername	The cornername.
/// @param	delta	  	The delta.
/// @param	color	  	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::fillCircleHelper(
	int16_t x0, int16_t y0,
	int16_t r,
	uint8_t cornername,
	int16_t delta,
	uint16_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		if (cornername & 0x1)
		{
			drawFastVLine(x0 + x, y0 - y, 2 * y + 1 + delta, color);
			drawFastVLine(x0 + y, y0 - x, 2 * x + 1 + delta, color);
		}
		if (cornername & 0x2)
		{
			drawFastVLine(x0 - x, y0 - y, 2 * y + 1 + delta, color);
			drawFastVLine(x0 - y, y0 - x, 2 * x + 1 + delta, color);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать залитый треугольник.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x0   	The x coordinate 0.
/// @param	y0   	The y coordinate 0.
/// @param	x1   	The first x value.
/// @param	y1   	The first y value.
/// @param	x2   	The second x value.
/// @param	y2   	The second y value.
/// @param	color	The color.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::fillTriangle(
	int16_t x0, int16_t y0,
	int16_t x1, int16_t y1,
	int16_t x2, int16_t y2,
	uint16_t color)
{

	int16_t a, b, y, last;

	// Sort coordinates by Y order (y2 >= y1 >= y0)
	if (y0 > y1) {
		swap(y0, y1); swap(x0, x1);
	}
	if (y1 > y2) {
		swap(y2, y1); swap(x2, x1);
	}
	if (y0 > y1) {
		swap(y0, y1); swap(x0, x1);
	}

	if (y0 == y2) { // Handle awkward all-on-same-line case as its own thing
		a = b = x0;
		if (x1 < a)
			a = x1;
		else if (x1 > b)
			b = x1;
		if (x2 < a)
			a = x2;
		else if (x2 > b)
			b = x2;
		drawFastHLine(a, y0, b - a + 1, color);
		return;
	}

	int16_t
		dx01 = x1 - x0,
		dy01 = y1 - y0,
		dx02 = x2 - x0,
		dy02 = y2 - y0,
		dx12 = x2 - x1,
		dy12 = y2 - y1,
		sa = 0,
		sb = 0;

	// For upper part of triangle, find scanline crossings for segments
	// 0-1 and 0-2.  If y1=y2 (flat-bottomed triangle), the scanline y1
	// is included here (and second loop will be skipped, avoiding a /0
	// error there), otherwise scanline y1 is skipped here and handled
	// in the second loop...which also avoids a /0 error here if y0=y1
	// (flat-topped triangle).
	if (y1 == y2)
		last = y1;   // Include y1 scanline
	else
		last = y1 - 1; // Skip it

	for (y = y0; y <= last; y++)
	{
		a = x0 + sa / dy01;
		b = x0 + sb / dy02;
		sa += dx01;
		sb += dx02;
		/* longhand:
		a = x0 + (x1 - x0) * (y - y0) / (y1 - y0);
		b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		*/
		if (a > b)
			swap(a, b);
		drawFastHLine(a, y, b - a + 1, color);
	}

	// For lower part of triangle, find scanline crossings for segments
	// 0-2 and 1-2.  This loop is skipped if y1=y2.
	sa = dx12 * (y - y1);
	sb = dx02 * (y - y0);
	for (; y <= y2; y++)
	{
		a = x1 + sa / dy12;
		b = x0 + sb / dy02;
		sa += dx12;
		sb += dx02;
		/* longhand:
		a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
		b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		*/
		if (a > b)
			swap(a, b);
		drawFastHLine(a, y, b - a + 1, color);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать залитый прямоугольник.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	x	 	Начальная координата X.
/// @param	y	 	Начальная координата Y.
/// @param	w	 	Ширина.
/// @param	h	 	Высота.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::fillRect(
	int16_t x, int16_t y,
	int16_t w, int16_t h,
	uint16_t color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT))
		return;
	if ((x + w - 1) >= LCD_WIDTH)
		w = LCD_WIDTH - x;
	if ((y + h - 1) >= LCD_HEIGHT)
		h = LCD_HEIGHT - y;

	HSPI.spiBegin();
	HSPI.spiLow(LCD_CS);

	setAddressWindow_(x, y, x + w - 1, y + h - 1);
	uint8_t colorBin[] = { (uint8_t)(color >> 8), (uint8_t)color };
	
	HSPI.writePattern(&colorBin[0], 2, (w * h));

	HSPI.spiHigh(LCD_CS);
	HSPI.spiEnd();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Нарисовать залитый прямоугольник с скругленными краями.
///
/// @author	Ers
/// @date	12.02.2016
///
/// @param	x   	Координата начала X.
/// @param	y	  	Координата начала Y.
/// @param	w	 	Длина по горизонтали.
/// @param	h	 	Длина по вертикали.
/// @param	r	 	Радиус скругления углов.
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::fillRoundRect(
	int16_t x, int16_t y,
	int16_t w, int16_t h,
	int16_t r,
	uint16_t color)
{
	fillRect(x + r, y, w - 2 * r, h, color);

	fillCircleHelper(x + w - r - 1, y + r, r, 1, h - 2 * r - 1, color);
	fillCircleHelper(x + r, y + r, r, 2, h - 2 * r - 1, color);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Залить весь экран.
///
/// @author	Ers
/// @date	11.02.2016
///
/// @param	color	Цвет.
////////////////////////////////////////////////////////////////////////////////////////////////////
void Gfx::fillScreen(uint16_t color)
{
	fillRect(0, 0, LCD_WIDTH, LCD_HEIGHT, color);
}



void Gfx::setTouchCalibration(uint16_t adc_x_0,	uint16_t adc_y_0, uint16_t adc_x_max, uint16_t adc_y_max)
{
	_touch->_touch_cal_x_0 = adc_x_0 ? adc_x_0 : (((int32_t)TOUCH_CAL_MARGIN) * TOUCH_ADC_MAX / LCD_WIDTH);
	_touch->_touch_cal_y_0 = adc_y_0 ? adc_y_0 : (((int32_t)TOUCH_CAL_MARGIN) * TOUCH_ADC_MAX / LCD_HEIGHT);
	_touch->_touch_cal_x_max = adc_x_max ? adc_x_max : (((int32_t)LCD_WIDTH - TOUCH_CAL_MARGIN) *  TOUCH_ADC_MAX / (2*LCD_WIDTH));
	_touch->_touch_cal_y_max = adc_y_max ? adc_y_max : (((int32_t)LCD_HEIGHT - TOUCH_CAL_MARGIN) * TOUCH_ADC_MAX / (2*LCD_HEIGHT));
}

lcd_point_t Gfx::getTouchPosition()
{
	lcd_point_t touch_point = _touch->getRawXY();
	if (touch_point.x != 0 && touch_point.y != 0)
	{
		switch (_rotation)
		{
		//LCD_ROTATION_DEGREE_0:
		//	{

		//	}
		//LCD_ROTATION_DEGREE_90:
		//	{

		//	}
		default:
			break;
		}
	}
}