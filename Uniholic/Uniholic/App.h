#pragma once

//#include <ctime>
#include <Arduino.h>
#include <HardwareSerial.h>

#include "UTime.h"
//#include "Uhspi.h"
//#include "Lcd.h"
#include "Gfx.h"



////////////////////////////////////////////////////////////////////////////////////////////////////
/// Пины подключения TFT LCD
///
/// @author	Ers
/// @date	24.01.2016
/// 	Pinout HSPI:
///			CS         -> GPIO15 (nodemcu_v1.0: D8)
///			D/C        -> GPIO2  (nodemcu_v1.0: D4)
///			SCK        -> GPIO14 (nodemcu_v1.0: D5)
///			SDI(MOSI)  -> GPIO13 (nodemcu_v1.0: D7)
///			SODI(MISO) -> GPIO12 (nodemcu_v1.0: D6)
////////////////////////////////////////////////////////////////////////////////////////////////////
struct pin_tft_str
{
	uint8_t _CS;	//TFT LCD pin CS -> GPIO15 (nodemcu_v1.0: D8)
	uint8_t _DC;	//TFT LCD pin D/C -> GPIO2  (nodemcu_v1.0: D4)
	uint8_t _SCK;	//TFT LCD pin SCK -> GPIO14 (nodemcu_v1.0: D5)
	uint8_t _SDI;	//TFT LCD pin SDI(MOSI) -> GPIO13 (nodemcu_v1.0: D7)
	uint8_t _SDO;	//TFT LCD pin SODI(MISO) -> GPIO12 (nodemcu_v1.0: D6)
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Пины подключения Touch LCD
///
/// @author	Ers
/// @date	24.01.2016
///		Pinout HSPI:
///			T_CS  -> GPIO16 (nodemcu_v1.0: D0)
///			T_IRQ -> GPIO03 (nodemcu_v1.0: RX)
///			T_CLK -> GPIO14 (nodemcu_v1.0: D5)
///			T_DIN -> GPIO13 (nodemcu_v1.0: D7)
///			T_DO  -> GPIO12 (nodemcu_v1.0: D6)
////////////////////////////////////////////////////////////////////////////////////////////////////
struct pin_touch_str
{
	uint8_t _T_CS;	//Touch LCD pin T_CS -> GPIO16 (nodemcu_v1.0: D0)
	uint8_t _T_IRQ;	//Touch LCD pin T_IRQ -> GPIO03 (nodemcu_v1.0: RX)
	uint8_t _T_CLK;	//Touch LCD pin T_CLK -> GPIO14 (nodemcu_v1.0: D5)
	uint8_t _T_DIN;	//Touch LCD pin T_DIN -> GPIO13 (nodemcu_v1.0: D7)
	uint8_t _T_DO;	//Touch LCD pin T_DO -> GPIO12 (nodemcu_v1.0: D6)
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Пины подключения I2C
///
/// @author	Ers
/// @date	24.01.2016
///		Pinout:
///			SDA -> GPIO4 (nodemcu_v1.0: D2)
///			SLC -> GPIO5 (nodemcu_v1.0: D1)
////////////////////////////////////////////////////////////////////////////////////////////////////
struct pin_i2c_str
{
	uint8_t _SDA;	//I2C pin SDA -> GPIO4 (nodemcu_v1.0: D2)
	uint8_t _SLC;	//I2C pin SLC -> GPIO5 (nodemcu_v1.0: D1)
};


class App
{
protected:
	

public:


	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Использовать режим вывода отладочной информации
	////////////////////////////////////////////////////////////////////////////////////////////////////
	bool _debug = true;

	bool _debug_core = true;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Скорость порта Serial для отладки.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	static const uint32_t SPEED_HARDWARE_SERIAL;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Пин подключения 1-Wire.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	static const uint8_t PIN_ONEWIRE;

	static const pin_tft_str PIN_TFT;
	static const pin_touch_str PIN_TOUCH;
	static const pin_i2c_str PIN_I2C;
	

	/// The debug port out.
	HardwareSerial* debug_port_out;

	/// The graphics.
	Gfx* _gfx;


	App();
	~App();

	template <typename Type> void Debug(Type message);
	template <typename Type> void Debugln(Type message);

	void Debug_tm();

	void InitHardwareSerial();
	void InitGfx();

	


};

//////////////////////////////////////////////////////////////////////////////////////////////////////
///// Debugs the given message.
/////
///// @author	Ers
///// @date	23.01.2016
/////
///// @tparam	Type	Type of the type.
///// @param	message	The message.
//////////////////////////////////////////////////////////////////////////////////////////////////////
template <typename Type>
void App::Debug(Type message)
{
	if (!(this->_debug)) return;
	(this->debug_port_out)->print(message);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
///// Debuglns the given message.
/////
///// @author	Ers
///// @date	23.01.2016
/////
///// @tparam	Type	Type of the type.
///// @param	message	The message.
//////////////////////////////////////////////////////////////////////////////////////////////////////
template <typename Type> 
void App::Debugln(Type message)
{
	if (!(this->_debug)) return;
	(this->debug_port_out)->println(message);
}

extern App UApp;
