#pragma once

#include "rgb565.h"

#include "Lcd.h"
#include "XPT2046.h"

#define swap(a, b) { int16_t t = a; a = b; b = t; }

//struct point_t
//{
//	uint8_t x;
//	uint8_t y;
//};

class Gfx
{
public:

	enum rotation_t
	{
		LCD_ROTATION_DEGREE_0,
		LCD_ROTATION_DEGREE_90,
		LCD_ROTATION_DEGREE_180,
		LCD_ROTATION_DEGREE_270
	};
	
	Lcd* _lcd;
	uint16_t LCD_WIDTH;
	uint16_t LCD_HEIGHT;
	uint8_t LCD_CS;
	uint8_t LCD_DC;

	XPT2046* _touch;
	uint8_t TOUCH_CS;
	uint8_t TOUCH_IRQ;


	Gfx();
	~Gfx();

	void setRotation(rotation_t r);
	void invertDisplay(boolean i);

	uint16_t color565(uint8_t r, uint8_t g, uint8_t b);

	void setAddressWindow_(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
	void setAddressWindow(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);

	void drawPixel(int16_t x, int16_t y, uint16_t color);
	void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);

	void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
	void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);

	void drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
	void drawCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint16_t color);
	void drawTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
	void drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
	void drawRoundRect(int16_t x0, int16_t y0, int16_t w, int16_t h, int16_t radius, uint16_t color);

	void fillCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
	void fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint16_t color);
	void fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
	void fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
	void fillRoundRect(int16_t x0, int16_t y0, int16_t w, int16_t h, int16_t radius, uint16_t color);
	void fillScreen(uint16_t color);



	void setTouchCalibration(uint16_t adc_x_0 ,	uint16_t adc_y_0 = 0, uint16_t adc_x_max = 0, uint16_t adc_y_max = 0);
	lcd_point_t getTouchPosition();


protected:

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// ������� ������ ������ � ������ ��������.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	int16_t	_width;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// ������� ������ ������ � ������ ��������.
	////////////////////////////////////////////////////////////////////////////////////////////////////
	int16_t _height;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// ����� �������� ������ ( 0�, 90�, 180�, 270� )
	////////////////////////////////////////////////////////////////////////////////////////////////////
	rotation_t _rotation;




	
};

